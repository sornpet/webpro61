<!DOCTYPE HTML>
<html>

<body>
	<p>Before the script.....</p>

	<script >
      function ask(question, yes, no){
      	if(confirm(question)) yes()
      		else no();
      }
      function showOK(){
      	alert("You agreed.");
      }
      function showCancel(){
      	alert("You canceled the execution");
      }
      ask("Do you agree?", showOK, showCancel);
		</script>
	<p>....After the script.</p>

</body>


</html>	